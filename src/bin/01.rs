use std::fs;
use std::cmp::max;

fn main() {
  let file_path = "inputs/1";
  let content_str = fs::read_to_string(file_path)
    .expect("Should have been able to read this!");

  let mut contents = content_str
    .split('\n')
    .collect::<Vec<&str>>();
       
    while (*contents.last().unwrap()).is_empty() {
      contents.truncate(contents.len()-1);
    }

  part_one(&contents);
  part_two(&contents);
}

fn part_one(contents: &Vec<&str>) {

    let mut total = 0;
    let mut biggest_total = 0;

    for line in contents.iter() {
      let parse_result = str::parse::<u32>(line);
      if let Ok(i) = parse_result {
        total += i;
      }
      else {
        biggest_total = max(biggest_total, total);
        total = 0;
      }
    }
    println!("Part one total = {}", biggest_total);
}

fn part_two(contents: &Vec<&str>) {

    let mut total = 0;
    let mut elf_totals = Vec::<u32>::new();

    for line in contents.iter() {
      let parse_result = str::parse::<u32>(line);
      if let Ok(i) = parse_result {
        total += i;
      }
      else {
        elf_totals.push(total);
        total = 0;
      }
    }

    elf_totals.sort();

    let final_three: u32 = elf_totals.iter().rev().take(3).sum();

    println!("Part two total = {}", final_three);  
}