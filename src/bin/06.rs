use common::{get_input, contains_duplicates};
use std::str::Chars;

fn main() {
  let inputs = get_input(6, false);
  let input = inputs.first().unwrap();
  println!("Part one result = {}", part_one(input));
  println!("Part two result = {}", part_two(input));
}

fn get_index_of_first_n_distinct_chars(input: &str, number: usize) -> usize {
  let mut index: usize = number;

  while index < input.len() {
    let arr = input.chars().skip(index-number).take(number).collect();

    let any_two_equal = contains_duplicates(&arr);
    if !any_two_equal {
      break;
    }
    index += 1;
  }

  index
}

fn part_one(input: &str) -> usize {
  get_index_of_first_n_distinct_chars(input, 4)
}

fn part_two(input: &str) -> usize {
  get_index_of_first_n_distinct_chars(input, 14)
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
      let inputs = get_input(6, true);
      let input = inputs.first().unwrap();
      assert_eq!(part_one(input), 7);
    }

    #[test]
    fn test_part_two() {
      let inputs = get_input(6, true);
      let input = inputs.first().unwrap();
      assert_eq!(part_two(input), 19);
    }    
   
}