use common::{
  get_input
};

#[derive(Debug,Clone)]
enum Direction {
  Left,
  Up,
  Right,
  Down
}

impl Direction {
  fn from_string(input: &str) -> Vec<Direction> {
    let whitespace_index = input.find(char::is_whitespace).unwrap();
    let (direction_str, count_str) = input.split_at(whitespace_index);
    let direction = match direction_str {
      "L" => Direction::Left,
      "U" => Direction::Up,
      "D" => Direction::Down,
      "R" => Direction::Right,
      _ => panic!("Direction parsing failed!")
    };    
    let count = count_str.trim().parse::<u32>().unwrap();    
    let mut result = Vec::new();
    for _ in 1..=count {
      result.push(direction.clone());
    }
    result
  }
}

#[derive(Debug,Clone,PartialEq)]
struct Position {
  x: isize,
  y: isize
}

impl Position {
  fn adjacent(&self, other: &Position) -> bool {
    let x_distance = (self.x-other.x).abs();
    let y_distance = (self.y-other.y).abs();
    x_distance <= 1 && y_distance <= 1
  }
}

struct Knot {
  position: Position,
  next: Option<Box<Knot>>,
  visited: Vec<Position>
}

impl Knot {
  fn new(position: &Position, length: u32) -> Self {
    let next = if length > 0 {
      Some(Box::new(Knot::new(position, length-1)))
    }
    else {
      None
    };
    Knot {
      position: (*position).clone(),
      next,
      visited: vec![(*position).clone()]
    }
  }

  fn get_last(&self) -> &Knot {
    if let Some(nextbox) = &self.next {
      (*nextbox).get_last()
    }
    else {
      self
    }
  }

  fn move_directly(&mut self, movement: &Direction) {
    self.apply_movement(movement);
    if let Some(nextbox) = &mut self.next {
      (*nextbox).parent_moved(&self.position);
    }
  }

  fn parent_moved(&mut self, new_parent_position: &Position) {
    
    if !self.position.adjacent(new_parent_position) {
      // if non-adjacent but in same row or column, move one step closer to head
      match(self.position.x).cmp(&new_parent_position.x) {
        std::cmp::Ordering::Less => {
          self.apply_movement(&Direction::Right);
        },
        std::cmp::Ordering::Greater => {
          self.apply_movement(&Direction::Left);
        },
        _ => (),
      }

      match(self.position.y).cmp(&new_parent_position.y) {
        std::cmp::Ordering::Less => {
          self.apply_movement(&Direction::Up);
        },
        std::cmp::Ordering::Greater => {
          self.apply_movement(&Direction::Down);
        },
        _ => (),
      }

      if !self.visited.contains(&self.position) {
        self.visited.push(self.position.clone());
      }

      if let Some(nextbox) = &mut self.next {
        (*nextbox).parent_moved(&self.position);
      }
    }
  }

  fn apply_movement(&mut self, movement: &Direction) {

    match movement {
        Direction::Left => self.position.x -= 1,
        Direction::Up => self.position.y +=1,
        Direction::Right => self.position.x += 1,
        Direction::Down => self.position.y -= 1,
    }    
  }  
}

fn main() {
  let inputs = get_input(9, false);
  
  println!("Part one result = {}", part_one(&inputs));
  println!("Part two result = {}", part_two(&inputs));
}

fn part_one(inputs: &[String]) -> usize {
  let movements: Vec<Direction> = inputs
  .iter()
  .flat_map(|s| Direction::from_string(s))
  .collect();

  let mut head = Knot::new(&Position{x:0,y:0}, 1);

  for movement in movements.iter() {
    head.move_directly(movement);
  }

  let result = head.get_last().visited.len();

  result
}

fn part_two(inputs: &[String]) -> usize {
  let movements: Vec<Direction> = inputs
  .iter()
  .flat_map(|s| Direction::from_string(s))
  .collect();

  let mut head = Knot::new(&Position{x:0,y:0}, 9);

  for movement in movements.iter() {
    head.move_directly(movement);
  }

  let result = head.get_last().visited.len();

  result
}

#[cfg(test)]
mod tests {
use super::*;

#[test]
fn test_part_one() {
  let inputs = get_input(9, true);
  assert_eq!(part_one(&inputs), 13);
}

#[test]
fn test_part_two() {
  let inputs = get_input(9, true);
  assert_eq!(part_two(&inputs), 1);
}

#[test]
fn actual_part_one() {
  let inputs = get_input(9, false);
  assert_eq!(part_one(&inputs), 6030);
}

#[test]
fn actual_part_two() {
  let inputs = get_input(9, false);
  assert_eq!(part_two(&inputs), 2545);
}   
}