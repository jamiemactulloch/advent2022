use clap::Parser;
use std::fs;
use reqwest;

#[derive(Parser,Default,Debug)]
struct Arguments {
  day: u8
}

fn main() -> ()
{
  let args = Arguments::parse();
  let day = args.day;

  let session_cookie_string = fs::read_to_string(".adventofcode.session")
    .expect("Session cookie not found in project root! Create file '.adventofcode.session' to proceed!");

  let host = "https://adventofcode.com";
  let path = format!("/2022/day/{}/input", day);
  let url = format!("{}{}", host, path);

  dbg!("Url = {}", &url);

  // Send the HTTP request.
  let client = reqwest::blocking::Client::new();

  // Send a GET request to the specified URL with the session cookie.
  let result = client.get(url)
      //.header("session", session_cookie_string)
      .header("cookie", format!("session={}",session_cookie_string))
      .send();

  if let Ok(response) = result {
    dbg!(&response);
    let response_body = response.text();

    if let Ok(mut body) = response_body {
      body = body.replace("\\n","\n");
      fs::write(format!("inputs/{}_actual", day), body)
        .expect("Was unable to write to file, because of reasons.");
    }
  }
}