use common::get_input;

#[derive(Debug, PartialEq, Clone)]
struct File {
  size: u32,
  name: String
}

#[derive(Debug, PartialEq, Clone)]
struct Directory {
  name: String,
  files: Vec<File>,
  child_dirs: Vec<Directory>
}

impl Directory {

  pub fn new(name: String) -> Directory {
    Directory {
      name: name,
      files: Vec::new(),
      child_dirs: Vec::new()
    }
  }

  pub fn add_file(&mut self, filename: &str, size: u32) {
    self.files.push(File{size:size, name:filename.into()});
  }

  pub fn add_child_dir(&mut self, dir: Directory) {
    self.child_dirs.push(dir);
  }

  pub fn get_directory_by_traversal(&mut self, dirs: &Vec<String>) -> &mut Directory {

    if dirs.len() == 1 && self.name == dirs[0] {
        return self;
    }

    let mut dirs = dirs.clone();
    dirs.remove(0);

    let next_name = dirs.first().unwrap();
    let next_dir = self.child_dirs
    .iter_mut()
    .find(|d|d.name == *next_name)
    .unwrap();

    return next_dir.get_directory_by_traversal(&dirs);
  }

  pub fn size(&self) -> u32 {
    let files_size: u32 = self.files.iter().map(|f| f.size)
    .sum();

    let dirs_size: u32 = self.child_dirs.iter().map(|d| d.size()).sum();

    let total = files_size + dirs_size;

    total
  }

  pub fn sizes(&self) -> Vec<(String,u32)> {
    let mut sizes = Vec::new();
    sizes.push((self.name.clone(),self.size()));

    if !self.child_dirs.is_empty() {
      let mut child_dirs = self.child_dirs.iter();
      while let Some(child) = child_dirs.next() {
        let subsizes = &mut (child.sizes());
        sizes.append(subsizes);
      }
    }

    sizes
  } 

}


#[derive(Debug)]
enum Line {
  Command(Command),
  Output(Output)
}

impl Line {
  pub fn from_string(input:&str) -> Line {
    if input.starts_with("$ ") {
      Line::Command(Command::from_string(input.chars().skip(2).collect()))
    }
    else {
      Line::Output(Output::from_string(input))
    }
  }
}

#[derive(Debug)]
enum Command {
  Ls,
  Cd(Directory)
}

impl Command {
  pub fn from_string(input: String) -> Command {
    match input.as_str()  {
      "ls" => Command::Ls,
      _ => Command::Cd(
        Directory::new(input.split_whitespace().last().unwrap().to_string())
      )
    }
  }
}

#[derive(Debug)]
struct Output {
  first: String,
  second: String
}

impl Output {
  pub fn from_string(input:&str) -> Output {
    let whitespace_index = input.find(char::is_whitespace).unwrap();
    let (first, second) = input.split_at(whitespace_index);
    Output{
      first: first.to_string(),
      second: second.trim().to_string()
    }
  }
}

fn main() {
  let inputs = get_input(7, true);
  println!("Part one result = {}", part_one(&inputs));
  println!("Part two result = {}", part_two(&inputs));
}

fn build_file_tree(lines: Vec<Line>) -> Directory {
  let mut current_dir_name_stack:Vec<String> = Vec::new();
  let mut rootdir = Directory::new("/".to_string());  
  let mut iterator = lines.iter();

  while let Some(line) = iterator.next() {
    match line {
      Line::Command(Command::Cd(d)) => {
        match d.name.as_str() {
          "/" => {
            current_dir_name_stack.clear();
            current_dir_name_stack.push("/".to_string());
          },
          ".." => {
            current_dir_name_stack.pop();
          },
          dir_name => {

            let parent_dir = rootdir.get_directory_by_traversal(&current_dir_name_stack);

            let existing_dir = parent_dir.child_dirs.iter().find(|d|d.name == dir_name);

            if existing_dir.is_none() {
              let new_dir = Directory::new(dir_name.to_string());
              parent_dir.add_child_dir(new_dir);
            }

            current_dir_name_stack.push(dir_name.to_string());
          }
        }
      },
      Line::Command(Command::Ls) => (),
      Line::Output(o) => {
        let current_dir = rootdir.get_directory_by_traversal(&current_dir_name_stack);
        let first_parsed = o.first.parse::<u32>();
        if let Ok(i) = first_parsed {
          // file.
          current_dir.add_file(o.second.as_str(),i);

        }
        else {
          // dir
          let new_dir = Directory::new(o.second.clone());
          current_dir.add_child_dir(new_dir)

        }
      }
    }
  }
  
  rootdir
}

fn part_one(inputs: &Vec<String>) -> u32 {
  let lines: Vec<Line> = inputs.iter().map(|s| Line::from_string(s)).collect();

  let filetree = build_file_tree(lines);

  let sizes = filetree.sizes();

  let sizes_filtered = sizes
    .into_iter()
    .filter(|(_,n)|*n <= 100000)
    .collect::<Vec<(String,u32)>>();

  let values_only = sizes_filtered
    .into_iter()
    .map(|(_,n)|n)
    .collect::<Vec<u32>>();

  let sum = values_only.iter().sum();
  sum
}

fn part_two(inputs: &Vec<String>) -> u32 {
  let total_space = 70000000;
  let free_space_required = 30000000;

  let lines: Vec<Line> = inputs.iter().map(|s| Line::from_string(s)).collect();
  let filetree = build_file_tree(lines);
  let sizes = filetree.sizes();
  let space_used = sizes.iter().find(|(name,_)| name == "/").unwrap().1;
  let free_space_actual = total_space - space_used;
  let amount_to_delete = free_space_required - free_space_actual;

  let smallest_deletable = sizes
    .iter()
    .map(|(_,size)|size)
    .filter(|&size| *size >= amount_to_delete)
    .min()
    .unwrap();

  *smallest_deletable
  
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
      let inputs = get_input(7, true);
      assert_eq!(part_one(&inputs), 95437);
    }

    #[test]
    fn test_part_two() {
      let inputs = get_input(7, true);
      assert_eq!(part_two(&inputs), 24933642);
    }    
 
    #[test]
    fn actual_part_one() {
      let inputs = get_input(7, false);
      assert_eq!(part_one(&inputs), 1118405);
    }
  
    #[test]
    fn actual_part_two() {
      let inputs = get_input(7, false);
      assert_eq!(part_two(&inputs), 12545514);
    }   

}