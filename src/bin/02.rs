use std::fs;
use itertools::Itertools;

#[derive(Debug, Clone)]
enum Move {
  Rock = 1,
  Paper = 2,
  Scissors = 3
}

#[derive(Debug)]
enum GameOutcome {
  Win = 6,
  Lose = 0,
  Draw = 3
}

struct Game {
  my_move: Move,
  their_move: Move
}

impl Game {
  fn new(my: Move, their: Move) -> Self {
    Game {
      my_move: my,
      their_move: their
    }
  }

  fn outcome(&self) -> GameOutcome {
    match (&self.my_move, &self.their_move) {
      (Move::Rock, Move::Paper) => GameOutcome::Lose,
      (Move::Rock, Move::Scissors) => GameOutcome::Win,
      (Move::Paper, Move::Rock) => GameOutcome::Win,
      (Move::Paper, Move::Scissors) => GameOutcome::Lose,
      (Move::Scissors, Move::Rock) => GameOutcome::Lose,
      (Move::Scissors, Move::Paper) => GameOutcome::Win,
      (_,_) => GameOutcome::Draw
    }
  }

  fn score(&self) -> u8 {
    self.my_move as u8 + self.outcome() as u8
  }

  fn from_string(input: &str) -> GameOutcome {
    match input {
      "X" => GameOutcome::Lose,
      "Y" => GameOutcome::Draw,
      "Z" => GameOutcome::Win,
      _ => panic!("from_string fed shitty input: {}", input)
    }
  }

  fn infer_my_move(their_move: &Move, outcome: &GameOutcome) -> Move {
    match (their_move, outcome) {
      (Move::Rock, GameOutcome::Win) => Move::Paper,
      (Move::Rock, GameOutcome::Lose) => Move::Scissors,
      (Move::Paper, GameOutcome::Win) => Move::Scissors,
      (Move::Paper, GameOutcome::Lose) => Move::Rock,
      (Move::Scissors, GameOutcome::Win) => Move::Rock,
      (Move::Scissors, GameOutcome::Lose) => Move::Paper,
      (_, GameOutcome::Draw) => (*their_move).clone(),
    }
  }

}

impl Move {
  fn from_string(input: &str) -> Move {
    match input {
      "A" | "X" => Move::Rock,
      "B" | "Y" => Move::Paper,
      "C" | "Z" => Move::Scissors,
      _ => panic!("from_string fed shitty input: {}", input)
    }
  }
}

// impl Clone for Move {
//   fn clone(&self) -> Move {
//     self.co
//   }
// }

fn main() {
  let file_path = "inputs/2_test";
  let content_str = fs::read_to_string(file_path)
    .expect("Should have been able to read this!");
    
  let mut contents = content_str
    .split('\n')
    .collect::<Vec<&str>>();

  while (*contents.last().unwrap()).is_empty() {
    contents.truncate(contents.len()-1);
  }

  println!("{:?}", &contents);

  part_one(&contents);
  part_two(&contents);
}

fn part_one(contents: &Vec<&str>) {

  let mut total_score: u16 = 0;
  for item in contents.iter() {

    let moves = item
    .split_whitespace()
    .map(|m| Move::from_string(m))
    .collect_tuple();
  
    if let Some((their_move, my_move)) = moves {

      let game = Game::new(my_move, their_move);
      let score = game.score();

      total_score += game.score() as u16;

      println!("{:?}, {:?}, {:?}, {:?}", game.my_move, game.their_move, game.outcome(), score);
    }
  }
  println!("Total score = {}", total_score);
}

fn part_two(contents: &Vec<&str>) {
  let mut total_score: u16 = 0;
  for item in contents.iter() {

    let round = item
    .split_whitespace()
    .collect_tuple();

    if let Some((move_str, outcome_str)) = round {
      let their_move = Move::from_string(move_str);
      let outcome = Game::from_string(outcome_str);

      let mut score = outcome as u16;

      let my_move = Game::infer_my_move(&their_move, &outcome);

      score += my_move as u16;

      println!("{:?}, {:?}, {:?}, {:?}", my_move, their_move, outcome, score);
      
      total_score += score;

    }
  }
  println!("Total score = {}", total_score);  
}

