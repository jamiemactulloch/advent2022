use common::{
  get_input, 
  letter_to_int, 
  get_index_of_char_from_matrix, 
  replace_char_in_matrix,
  scale_up_matrix
};
use minifb::{InputCallback, Key, Menu, Scale, Window, WindowOptions};

static DAY_NUMBER : u8 = 12;

#[derive(Debug, PartialEq, Clone)]
enum Movement {
  Up,
  Down,
  Left,
  Right
}

#[derive(Debug, PartialEq, Clone)]
struct Walk {
  visited: Vec<(usize,usize)>
}

fn get_start(grid: &Vec<Vec<char>>) -> Option<(usize,usize)> {
  get_index_of_char_from_matrix(grid,'S')
}

fn get_end(grid: &Vec<Vec<char>>) -> Option<(usize,usize)> {
  get_index_of_char_from_matrix(grid,'E')
}

fn get_matrix(strings: &Vec<String>) -> Vec<Vec<char>> {
  let result = strings
  .clone()
  .into_iter()
  .map(|s| s.chars().collect::<Vec<char>>())
  .collect();
  result
}

fn traversable(start: char, end: char) -> bool {
  let start_int = letter_to_int(start).unwrap();
  let end_int = letter_to_int(end).unwrap();

  let diff = end_int as i8 - start_int as i8;
  (0..=1).contains(&diff)
}

fn can_move(grid: &Vec<Vec<char>>, movement: &Movement, x: usize, y: usize) -> bool {
  let start = grid[y][x];
  let end: char;

  match movement {
    Movement::Up => {
      if y == 0 { return false; }
      end = grid[y-1][x];
    },
    Movement::Down => {
      if y+1 == grid.len() { return false; }
      end = grid[y+1][x];
    },
    Movement::Left => {
      if x == 0 { return false; }
      end = grid[y][x-1];
    },
    Movement::Right => {
      if x+1 == grid[0].len() { return false; }
      end = grid[y][x+1];
    },
  }
  let result = traversable(start,end);
  result
}

fn should_move(
  movement: &Movement, 
  x: usize, 
  y: usize,
  explored: &Vec::<(usize, usize)>,
  visited: &Vec::<(usize, usize)>)
 -> bool {
    !explored.contains(&do_move(movement, x, y))
    &&
    !visited.contains(&do_move(movement, x, y))
}

fn do_move(movement: &Movement, x: usize, y: usize) -> (usize,usize) {
  match movement {
    Movement::Up => (x,y-1),
    Movement::Down => (x,y+1),
    Movement::Left => (x-1,y),
    Movement::Right => (x+1,y),
  }
}

// let n = 1
// While true

// n++
// endwhile

// Try to go right, can I go right?
// If no go clockwise, so down next. 
// Once I get a yes, have I visited it already?
// Repeat clockwise
// Once I have two yeses, move there.
// If I've exhausted 
// Am i at the target?
// If yes, print route, route length and terminate.
// If no

// Traverse to next reachable square not in explored vec, preferring the following order: Right, Bottom, Left, Up
// If traversed square is target,  break, print index as route length, as well as route to target.
// Else traverse next (clockwise)
// If no traversables left, we must be at a dead-end. Add square to explored vec.
  
fn recursive_thing(
  grid: &Vec<Vec<char>>, 
  start: (usize, usize), 
  end: (usize, usize), 
  max_walk_length: i32,
  current_walk_length: i32,
  explored: &mut Vec::<(usize,usize)>,
  visited: &Vec::<(usize,usize)>
) 
  -> Option<Walk> {
    let directions = vec![
      Movement::Right,
      Movement::Down,
      Movement::Left,
      Movement::Up
    ];
  
    let (current_x ,current_y)= start;
    let mut has_moved = false;
  
    for direction in directions.iter() {
      if can_move(&grid, direction, current_x, current_y)
      && should_move(direction, current_x, current_y, explored, visited) {
        let new_pos = do_move( direction, current_x, current_y);
        let mut visited_sub = visited.clone();
        visited_sub.push(new_pos);
        has_moved = true;
        let mut items = visited_sub.iter();
        while let Some(item) = items.next() {
          print!("{:?}->", item);
        }
        println!();
        if new_pos == end {
          println!("We found the end! Length = {}", current_walk_length);
          return Some(Walk{ visited: visited_sub });
        }
        else if max_walk_length == current_walk_length {
          //println!("Not at the end yet. Current pos = {},{}", current_x, current_y);
        }
        else {
          return recursive_thing(grid, new_pos, end,  max_walk_length, current_walk_length+1, explored, &visited_sub);
        }
      }
    }
    if !has_moved {
      explored.push((current_x,current_y));
    }    
    return None;
  }

fn get_route(grid: &Vec<Vec<char>>, start: (usize, usize), end: (usize, usize)) -> Option<Walk> {
  let mut n = 1;
  let mut explored = Vec::new();
  let max_route_length = grid.len() as i32 * grid[0].len() as i32;

  loop {
    let result = recursive_thing(grid, start, end,  n, 1, &mut explored, &Vec::new());
    if let Some(_) = result {
      return result;
    }
    n += 1;
    if n >= std::i32::MAX /*max_route_length*/ {
      // sanity failsafe
      println!("We couldn't find a route.");
      return None;
    }
  }
}

fn colourise_grid(grid: &Vec<Vec<char>>, start: (usize, usize), end: (usize, usize)) -> Vec<Vec<u32>> {
  let mut result: Vec<Vec<u32>> = grid.clone().into_iter()
  .map(|row| row.iter().map(
    |el| colour_map(*el)
    ).collect()
  ).collect();

  result[start.1][start.0] = colour_map('S');
  result[end.1][end.0] = colour_map('E');

  result
}

fn colour_map(chr: char) -> u32 {
  match chr {
    'a' => 0x000000,
    'b' => 0x09d89d,
    'c' => 0x13b13a,
    'd' => 0x1d89d7,
    'e' => 0x276274,
    'f' => 0x313b11,
    'g' => 0x3b13ae,
    'h' => 0x44ec4b,
    'i' => 0x4ec4e8,
    'j' => 0x589d85,
    'k' => 0x627622,
    'l' => 0x6c4ebf,
    'm' => 0x76275c,
    'n' => 0x7ffff9,
    'o' => 0x89d896,
    'p' => 0x93b133,
    'q' => 0x9d89d0,
    'r' => 0xa7626d,
    's' => 0xb13b0a,
    't' => 0xbb13a7,
    'u' => 0xc4ec44,
    'v' => 0xcec4e1,
    'w' => 0xd89d7e,
    'x' => 0xe2761b,
    'y' => 0xf62755,
    'z' => 0xffffff,
    'E' => 0x0096ff,
    'S' => 0xee4b2b,
    _ => panic!("Oh no!")
  }
}


fn get_buffer(grid: &Vec<Vec<char>>, start: (usize,usize), end: (usize,usize), scale: u16) -> Vec<u32> {
  let colorised = colourise_grid(grid, start, end);
  let result = scale_up_matrix(&colorised, scale).into_iter().flatten().collect();
  result
}

fn step(
  grid: &Vec<Vec<char>>, 
  terminals: &mut Vec::<Walk>,
  visited: &mut Vec::<(usize,usize)>
)  {
    let directions = vec![
      Movement::Right,
      Movement::Down,
      Movement::Left,
      Movement::Up
    ];
  
    let clone = terminals.clone();
    for point in clone.into_iter() {
      let (current_x, current_y) = point.visited.last().unwrap().clone();
      for direction in directions.iter() {
        if can_move(&grid, direction, current_x, current_y) {
          let new_pos = do_move( direction, current_x, current_y);
          if !visited.contains(&new_pos) {
//        if should_move(direction, current_x, current_y, visited, visited) {
            visited.push(new_pos);
            let mut new_walk = Walk { visited: point.visited.clone() };
            new_walk.visited.push(new_pos);
            terminals.push(new_walk);
            let mut items = point.visited.iter();
            if let Some(el) = items.next() {
              print!("{:?}", el);
            }
            while let Some(item) = items.next() {
              print!("->{:?}", item);
            }
            println!();
          }
        }
      }
      let index = terminals.iter().position(|el| *el == point).unwrap();
      terminals.remove(index);
    }
  
  }

fn get_route_new(grid: &Vec<Vec<char>>, start: (usize, usize), end: (usize, usize)) -> Option<Walk> {
  let mut n = 1;
  let mut terminals = Vec::new();
  let start = Walk {visited: vec![start]}; 
  terminals.push(start.clone());
  let mut visited = start.visited.clone();
  let max_route_length = grid.len() as i32 * grid[0].len() as i32;

  loop {
    step(grid, &mut terminals, &mut visited);
    if terminals.is_empty() {
      println!("Hit a dead end, debug.");
      break;
    }
    for point in terminals.iter() {
      if point.visited.contains(&end) {
        return Some(point.clone());
      }
    }

    n += 1;
    if n >= max_route_length { //std::i32::MAX /**/ 
      println!("Running forever, debug.");
      break;
      // sanity failsafe
    }
  }
  return None;

}


fn part_one(inputs: &Vec<String>) -> u32 { 
  let mut grid = get_matrix(inputs);
  let start = get_start(&grid).unwrap();
  replace_char_in_matrix(&mut grid, 'S', 'a');
  let end = get_end(&grid).expect("Couldn't get end position");
  replace_char_in_matrix(&mut grid, 'E', 'z');

  println!("Start = {:?}", start);
  println!("End = {:?}", end);

  let scale = 3;
  let height = grid.len() * scale;
  let width = grid[0].len() * scale;
  let mut buffer: Vec<u32> = get_buffer(&grid, start, end, scale as 16);

  let mut window = Window::new(
      "Menu Test - Press ESC to exit",
      width,
      height,
      WindowOptions {
          resize: true,
          scale: Scale::X1,
          ..WindowOptions::default()
      },
  )
  .expect("Unable to Open Window");

  while window.is_open() && !window.is_key_down(Key::Escape) {
        // We unwrap here as we want this code to exit if it fails
        window.update_with_buffer(&buffer, width, height).unwrap();
  }

  let result = get_route_new(&grid, start, end).expect("Oh no we failed");
  let length = result.visited.len()-1; // minus one to remove start node.
    println!("Minimum route length = {}", length);

  // test
  length as u32
  // actual:
  // not 70
}

fn part_two(inputs: &Vec<String>) -> u32 {
  0
}

fn main() {
  let inputs = get_input(DAY_NUMBER, false);
  println!("Part one result = {}", part_one(&inputs));
  println!("Part two result = {}", part_two(&inputs));
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn generic_tests() {
      
    }

    #[test]
    fn test_part_one() {
      let inputs = get_input(DAY_NUMBER, true);
      assert_eq!(part_one(&inputs), 31);
    }

    #[test]
    fn test_part_two() {
      let inputs = get_input(DAY_NUMBER, true);
      assert_eq!(part_two(&inputs), 31);
    }    
 
    #[test]
    fn actual_part_one() {
      let inputs = get_input(DAY_NUMBER, false);
      assert_eq!(part_one(&inputs), 31);
    }
  
    #[test]
    fn actual_part_two() {
      let inputs = get_input(DAY_NUMBER, false);
      assert_eq!(part_two(&inputs), 31);
    }   

}