use common::{
    get_input, 
    transpose_matrix, 
    max_value_in_matrix, 
    many_matrix_product, 
    reverse_matrix_elementwise, 
    flatten_matrix, 
    many_vector_elementwise_or
};

fn main() {
  let inputs = get_input(8, false);

  println!("Part one result = {}", part_one(&inputs));
  println!("Part two result = {}", part_two(&inputs));
}

fn get_grid(inputs: &Vec<String>) ->Vec<Vec<u32>> {
  let grid: Vec<Vec<u32>> = inputs
    .iter()
    .map(
      |s| s.chars().map(
        |c| c.to_digit(10).unwrap_or_else(|| panic!("Conversion to int failed {}", c))
      )
      .collect::<Vec<u32>>()
    )
    .collect();
  grid
}

fn get_visible_array(row: &Vec<u32>) -> Vec<bool> {
  row
  .iter()
  .scan(std::i32::MIN, |largest:&mut i32, number| {
    // Check if the current number is the largest seen so far
    let is_largest = (*number as i32) > *largest;
      
    // Update the largest integer seen so far
    //if (*number as i32) > *largest {
    if is_largest {
      *largest = *number as i32;
    }


    // Return the result as an iterator element
    Some(is_largest)
  })
  .collect()
}

fn get_visible_grid(grid: &Vec<Vec<u32>>) -> Vec<Vec<bool>> {
  grid
  .iter()
  .map(get_visible_array)
  .collect()
}

fn proximity_to_non_small_value(index: usize, row: &Vec<u32>) -> u32 {
  if index >= row.len() - 1 {
    return 0;
  }
  let val = row[index];
  for i in index+1..row.len() {
    if row[i] >= val {
      return (i-index) as u32;
    }
  }
  ((row.len() - 1) - index) as u32
}

fn blocking_right(x: usize, y: usize, grid: &Vec<Vec<u32>>) -> u32 {
  let row = &grid[y];
  proximity_to_non_small_value(x, row)
}

fn blocking_left(x:usize, y:usize, grid: &Vec<Vec<u32>>) -> u32 {
  let row: Vec<u32> = grid[y].clone().into_iter().rev().collect();
  let index = (row.len()-1)-x;
  proximity_to_non_small_value(index, &row)
}

fn blocking_down(x:usize, y:usize, grid: &Vec<Vec<u32>>) -> u32 {
  let transposed = transpose_matrix(grid);
  let row = &transposed[x];
  proximity_to_non_small_value(y, row)
}

fn blocking_up(x:usize, y:usize, grid: &Vec<Vec<u32>>) -> u32 {
  let transposed = transpose_matrix(grid);
  let row: Vec<u32> = transposed[x].clone().into_iter().rev().collect();
  proximity_to_non_small_value((row.len()-1)-y, &row)
}

fn blocking_right_row(row: &Vec<u32>) -> Vec<u32> {
  (*row)
  .clone()
  .into_iter()
  .enumerate()
  .map(|(i,_)| proximity_to_non_small_value(i, row))
  .collect()
}

fn blocking_right_grid(grid: &Vec<Vec<u32>>) -> Vec<Vec<u32>> { 
  
  
  grid
  .clone()
  .into_iter()
  .map(|v| blocking_right_row(&v))
  .collect()
}

fn part_one(inputs: &Vec<String>) -> u32 {

  let west = get_grid(inputs); 
  let from_the_west = get_visible_grid(&west);

  let north = transpose_matrix(&west);
  let from_the_north = get_visible_grid(&north);
  let north_truths_westernised = transpose_matrix(&from_the_north);
  
  let east = reverse_matrix_elementwise(&west);
  let from_the_east = get_visible_grid(&east);
  let east_truths_westernised = reverse_matrix_elementwise(&from_the_east);
  
  let south = reverse_matrix_elementwise(&north);
  let from_the_south = get_visible_grid(&south);
  let south_truths_westernised = transpose_matrix(&reverse_matrix_elementwise(&from_the_south));

  let truth_grid = vec![
    flatten_matrix(&from_the_west),
    flatten_matrix(&east_truths_westernised),
    flatten_matrix(&north_truths_westernised),
    flatten_matrix(&south_truths_westernised)
  ];

  let final_truths = many_vector_elementwise_or(truth_grid);

  let total = final_truths.iter().filter(|&&x| x).count();

  total as u32
}

fn method_one(inputs: &Vec<String>) -> u32 {
  let west = get_grid(inputs);

  let mut val_grid = Vec::new();

  for i in 0..west[0].len() {
    let mut row = Vec::new();
    for j in 0..west.len() {
      let total: u32 = vec![
        blocking_up(i,j,&west),
        blocking_down(i,j,&west),
        blocking_left(i,j,&west),
        blocking_right(i,j,&west)
      ].iter().product();
      row.push(total);
    }
    val_grid.push(row);
  }

  let maxes: Vec<u32> = val_grid.into_iter().map(|v| v.into_iter().max().unwrap()).collect();
  

  maxes.into_iter().max().unwrap()
}

fn method_two(inputs: &Vec<String>) -> u32 {
  let west = get_grid(inputs);
  let west_projection = blocking_right_grid(&west);

  let north = transpose_matrix(&west);
  let north_projection = blocking_right_grid(&north);
  let north_realigned = transpose_matrix(&north_projection);


  let east = reverse_matrix_elementwise(&west);
  let east_projection = blocking_right_grid(&east);
  let east_realigned = reverse_matrix_elementwise(&east_projection);

  let south = reverse_matrix_elementwise(&north);
  let south_projection = blocking_right_grid(&south);
  let south_realigned = transpose_matrix(&reverse_matrix_elementwise(&south_projection));

  let matrices = vec![
    west_projection,
    north_realigned,
    east_realigned,
    south_realigned
  ];

  let product_grid = many_matrix_product(&matrices);

  max_value_in_matrix(&product_grid)
}

fn part_two(inputs: &Vec<String>) -> u32 {

  let max = method_two(inputs);
  println!("Max 2 = {}", max);

//  let max2 = method_one(&inputs);
//  println!("Max 1 = {}", max2);
  max
  
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_part_one() {
    let inputs = get_input(8, true);
    assert_eq!(part_one(&inputs), 21);
  }

  #[test]
  fn test_part_two() {
    let inputs = get_input(8, true);
    assert_eq!(part_two(&inputs), 8);
  }
  
  #[test]
  fn actual_part_one() {
    let inputs = get_input(8, false);
    assert_eq!(part_one(&inputs), 1789);
  }

  #[test]
  fn actual_part_two() {
    let inputs = get_input(8, false);
    assert_eq!(part_two(&inputs), 314820);
  }   
}