use common::{get_input, letter_to_int, get_index_of_char_from_matrix};


#[derive(Debug, PartialEq, Clone)]
enum Movement {
  Up,
  Down,
  Left,
  Right
}

#[derive(Debug)]
struct Route {
  x: usize, y:usize,
  end: (usize,usize),
  next_options: Vec<Route>,
  visited: Vec<(usize,usize)>
}

impl Route {
  fn new (x: usize, y: usize, end: (usize,usize), visited: &Vec<(usize,usize)>) -> Route {
    let mut visited = visited.clone();
    visited.push((x,y));
    Route {
      x: x,
      y: y,
      end: end,
      next_options: Vec::new(),
      visited: visited.clone()
    }
  }

  fn from_movement(&self, mv: &Movement) -> Route {
    match mv {
      Movement::Up => Route::new(self.x,self.y-1, self.end, &self.visited),
      Movement::Down => Route::new(self.x,self.y+1, self.end, &self.visited),
      Movement::Left => Route::new(self.x-1,self.y, self.end, &self.visited),
      Movement::Right => Route::new(self.x+1,self.y, self.end, &self.visited),
    }
  }

  fn is_end(&self) -> bool {
    (self.x, self.y) == (self.end)
  }

  fn has_visited(&self, x: usize, y:usize) -> bool {
    self.visited.contains(&(x,y))
  }

  fn available_directions(&self, grid: &Vec<Vec<char>>) -> Vec<Movement> {
    let mut movements = 
    vec![
      Movement::Up,
      Movement::Down,
      Movement::Left,
      Movement::Right
    ];

    movements = movements
      .into_iter()
      .filter(|mv| 
        can_move(grid, &mv, self.x, self.y)
        && {
          let lookahead = self.from_movement(&mv);
          !self.has_visited(lookahead.x, lookahead.y)
      })
      .collect();

    movements
  }

  fn traversal(&mut self, grid: &Vec<Vec<char>>) -> Option<usize> {
    if !self.is_end() {

      let mut unvisited = self.unvisited_neighbours(grid);
      self.next_options.append(&mut unvisited);

      for next in self.next_options.iter_mut() {
        next.traversal(grid);
      }
    }

    if self.is_end() {
      self.print_journey();
    }
    return None;

  }

  fn unvisited_neighbours(&self, grid: &Vec<Vec<char>>) -> Vec<Route> {
    let mut result = Vec::new();
    let movements = self.available_directions(grid);
    for mv in movements {
      result.push(self.from_movement(&mv));
    }
    result
  }

  fn print_journey(&self) {
    let mut items = self.visited.iter();
    while let Some(item) = items.next() {
      print!("{:?}->", item);
    }
    println!("Done. Number of steps = {}", self.visited.len()-1);
  }
  

}

fn get_matrix(strings: &Vec<String>) -> Vec<Vec<char>> {
  let result = strings
  .clone()
  .into_iter()
  .map(|s| s.chars().collect::<Vec<char>>())
  .collect();
  result
}

fn traversable(start: char, end: char) -> bool {
  let start_int = letter_to_int(start).unwrap();
  let end_int = letter_to_int(end).unwrap();

  let diff = end_int as i8 - start_int as i8;
  (0..=1).contains(&diff)
}

fn shortest_route(grid: &Vec<Vec<char>>, start_x: usize,start_y: usize,end_x: usize,end_y: usize) -> usize {
  let mut loop_count = 1;
  let ceiling = (grid.len()-1) * (grid[0].len()-1);
  let mut result = ceiling;
  let mut moves = Vec::new();
  moves.push(Route::new(start_x,start_y, (end_x,end_y), &Vec::new()));
  
  while loop_count < ceiling {
    let mut all_neighbours= Vec::new();
    let mut iterator = moves.iter();
    for mov in moves.iter() {
      let mut these_neighbours = mov.unvisited_neighbours(grid);
      let mut neighbours_iter = these_neighbours.iter();
      for n in these_neighbours.iter() {
        if n.is_end() {
          n.print_journey();
          return loop_count;
        }
      }
      all_neighbours.append(&mut these_neighbours);
    }

    moves = all_neighbours;
    if moves.len() == 0 {
      return std::usize::MAX;
    }
    loop_count += 1;
  }

  result
}

fn can_move(grid: &Vec<Vec<char>>, movement: &Movement, x: usize, y: usize) -> bool {
  let start = grid[y][x];
  let end: char;

  match movement {
    Movement::Up => {
      if y == 0 { return false; }
      end = grid[y-1][x];
    },
    Movement::Down => {
      if y+1 == grid.len() { return false; }
      end = grid[y+1][x];
    },
    Movement::Left => {
      if x == 0 { return false; }
      end = grid[y][x-1];
    },
    Movement::Right => {
      if x+1 == grid[0].len() { return false; }
      end = grid[y][x+1];
    },
  }
  let result = traversable(start,end);
  result
}

fn get_start(grid: &Vec<Vec<char>>) -> Option<(usize,usize)> {
  get_index_of_char_from_matrix(grid,'S')
}

fn get_end(grid: &Vec<Vec<char>>) -> Option<(usize,usize)> {
  get_index_of_char_from_matrix(grid,'E')
}


fn replace(matrix: &mut Vec<Vec<char>>, old_char: char, new_char: char) {
  *matrix = (*matrix)
  .iter_mut()
  .map(|row| 
      row
      .iter_mut()
      .map(|&mut c| if c == old_char {new_char} else {c})
      .collect::<Vec<char>>()
  )
  .collect();
}

fn part_one(inputs: &Vec<String>) -> u32 {
  let mut grid = get_matrix(inputs);
  let start = get_start(&grid).unwrap();
  replace(&mut grid, 'S', 'a');
  let end = get_end(&grid).expect("Couldn't get end position");
  replace(&mut grid, 'E', 'z');

  println!("Start = {:?}", start);
  println!("End = {:?}", end);

  let mut routes = Route::new(start.0, start.1, end, &Vec::new());
  let result = shortest_route(&grid, start.0, start.1, end.0, end.1);
  //routes.traversal(&grid);
  let thing = result;
   
  0
}

fn part_two(inputs: &Vec<String>) -> u32 {
  0
}
fn main() {
  let inputs = get_input(12, false);
  println!("Part one result = {}", part_one(&inputs));
  println!("Part two result = {}", part_two(&inputs));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn generic_tests() {
      
    }

    #[test]
    fn test_part_one() {
      let inputs = get_input(12, true);
      assert_eq!(part_one(&inputs), 31);
    }

    #[test]
    fn test_part_two() {
      let inputs = get_input(12, true);
      assert_eq!(part_two(&inputs), 31);
    }    
 
    #[test]
    fn actual_part_one() {
      let inputs = get_input(12, false);
      assert_eq!(part_one(&inputs), 31);
    }
  
    #[test]
    fn actual_part_two() {
      let inputs = get_input(12, false);
      assert_eq!(part_two(&inputs), 31);
    }   

}