use common::{
  get_input
};

#[derive(Debug, Clone)]
enum Instruction {
  Addx(i32),
  Noop
}

impl Instruction {
  fn from_string(input: &str) -> Instruction {
    if input == "noop" {
      Instruction::Noop
    }
    else {
      let whitespace_index = input.find(char::is_whitespace).unwrap();
      let (_, val) = input.split_at(whitespace_index);
      let n = val.trim().parse::<i32>().expect("Parsing int failed in addx instruction");
      Instruction::Addx(n)
    }
  }
}

fn main() {
  let inputs = get_input(10, false);

  println!("Part one result = {}", part_one(&inputs));
}

fn get_register_history(instructions: &Vec<Instruction>) -> Vec<(i32,i32)> {
  let mut cycle_count = 1;
  let mut x:i32 = 1;
  let mut reg_history = Vec::new();
  let mut queued: Option<Instruction> = None;
  let mut instruction_set = instructions.clone().into_iter();
  let mut display = vec!['.'; 241];

  loop {
    reg_history.push((cycle_count, x));
    
    let cycle_normalised = (cycle_count-1) % 40;
    if (x-1..=x+1).contains(&cycle_normalised) {
      display[cycle_count as usize -1] = '#';
    }   

    if let Some(Instruction::Addx(n)) = queued {
      x += n;
      queued = None;
    }
    else {
      if let Some(ins) = instruction_set.next() {
        match ins {
          Instruction::Addx(_) => {
            queued = Some(ins.clone());
          }
          Instruction::Noop => ()
        }
      }
      else {
        break;
      }
    }
    cycle_count += 1;
  }

  let mut chunks = display.chunks(40);
  while let Some(chunk) = chunks.next() {
    println!("{:?}", chunk.iter().collect::<String>());
  }

  reg_history
}

fn part_one(inputs: &Vec<String>) -> i32 {
  let instruction_set = inputs
    .into_iter()
    .map(|s| Instruction::from_string(s))
    .collect();

  let register_history = get_register_history(&instruction_set);

  let products = register_history.into_iter()
  .filter(|(c,_)|*c >= 20 && ((*c)-20) % 40 == 0)
  .map(|(c,x)|x*c)
  .sum::<i32>();

  products

}


#[cfg(test)]
mod tests {
use super::*;

#[test]
fn test_part_one() {
  let inputs = get_input(10, true);
  assert_eq!(part_one(&inputs), 13140);
}

#[test]
fn actual_part_one() {
  let inputs = get_input(10, false);
  assert_eq!(part_one(&inputs), 11780);
}

}