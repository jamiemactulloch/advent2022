use common::{get_input, two_vector_shared_elements, letter_to_int};
use itertools::Itertools;
use std::env::args;

#[derive(Debug)]
struct Rucksack {
  first: Vec<char>,
  second: Vec<char>
}

impl Rucksack {
  fn from_string(input: &str) -> Rucksack {
    let mut chars = input.chars().collect::<Vec<char>>();
    let second = chars.split_off(chars.len()/2);
    let first = chars;

    Rucksack {
      first, 
      second
    }

  }

  fn all(&self) -> Vec<char> {
    let mut out = self.first.clone();
    out.append(&mut self.second.clone());
    out
  }

  fn common_items(&self) ->  Option<char> {
    let common_items = two_vector_shared_elements(&self.first, &self.second);
    let only_item = common_items.first();
    match only_item {
      Some(&c) => Some(c),
      None => None
    }
  }
}

impl Clone for Rucksack {
  fn clone(&self) -> Rucksack {
    Rucksack {
      first: self.first.clone(),
      second: self.second.clone()
    }
  }
}

#[derive(Debug)]
struct Group {
  first: Rucksack,
  second: Rucksack,
  third: Rucksack
}

impl Group {
  pub fn new(one: &Rucksack, two: &Rucksack, three: &Rucksack) -> Group {
    Group {
      first: (*one).clone(),
      second: (*two).clone(),
      third: (*three).clone()
    }
  }

  pub fn common_item(&self) ->Option<char> {
    let common_items = two_vector_shared_elements(&two_vector_shared_elements(&self.first.all(), &self.second.all()), &self.third.all());
    let only = common_items.first();
    match only {
      Some(&c) => Some(c),
      None => None
    }

  }
}

fn main() {
  let args: Vec<String> = args().collect();
  let rucksack_strings = get_input(3, false);
  part_one(&rucksack_strings);
  part_two(&rucksack_strings);
}

fn part_one(rucksack_strings: &Vec<String>) {
  let mut total = 0;
  for rucksack_str in rucksack_strings.iter() {

    let rucksack = Rucksack::from_string(rucksack_str);

    let common_items = rucksack.common_items();

    let mut subtotal = 0;
    for &character in common_items.iter() {
      if let Some(i) = letter_to_int(character) {
        subtotal += i;
      }
    }

    total += subtotal;

    println!("Total = {}, Subtotal = {}", total, subtotal);
  }
}

fn part_two(rucksack_strings: &Vec<String>) {
  let mut total = 0;
  let groups = rucksack_strings
  .chunks(3)
  .map(|chunk| vec![
    Rucksack::from_string(&chunk[0]),
    Rucksack::from_string(&chunk[1]),
    Rucksack::from_string(&chunk[2])])
  .map(|rucksacks| Group::new(&rucksacks[0],&rucksacks[1],&rucksacks[2]))
  .collect_vec();

  for group in groups.iter() {
    let common_item = group.common_item();
    if let Some(c) = common_item {
      if let Some(i) = letter_to_int(c) {
        total += i;
      }
    }
  }

  println!("Total priorities = {}", total);
}