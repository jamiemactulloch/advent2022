use std::collections::HashSet;
use common::{get_input, two_vector_shared_elements};

fn main() {
  let inputs = get_input(4, false);
  println!("Part one result = {}", part_one(&inputs));
  println!("Part two result = {}", part_two(&inputs));
}

fn make_range(input: &str)-> Vec<u8> {
  let mut iter = input.split('-');
  let lower = iter.next().unwrap().parse::<u8>().unwrap();
  let suss = iter.next();
  if let Some(s) = suss {
    let higher = s.parse::<u8>().unwrap();
    (lower..=higher).collect::<Vec<u8>>()
  }
  else {
    panic!("oh no");
  }
  // let higher = iter.next().unwrap().parse::<u8>().unwrap();
  // (lower..=higher).collect::<Vec<u8>>()

}

fn part_one(inputs: &Vec<String>) -> u16 {
  let mut total = 0;
  for input in inputs.iter() {
    let mut iter = input.split(',');
    let (first, second) = (iter.next().unwrap(), iter.next().unwrap());
    let first_range = make_range(first);
    let second_range = make_range(second);

    let first_hashset = first_range.into_iter().collect::<HashSet<u8>>();
    let second_hashset = second_range.into_iter().collect::<HashSet<u8>>();

    if first_hashset.is_subset(&second_hashset) || second_hashset.is_subset(&first_hashset) {
      total += 1;
    }
  }
  total
}


fn part_two(inputs: &Vec<String>) -> u16 {
  let mut total = 0;
  let mut count = 0;
for input in inputs.iter() {
  let mut iter = input.split(',');
  let (first, second) = (iter.next().unwrap(), iter.next().unwrap());
  let first_range = make_range(first);
  let second_range = make_range(second);

  if !two_vector_shared_elements(&first_range,&second_range).is_empty() {
    total += 1;
  }
  count += 1;

}

  total
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
      let inputs = get_input(4, true);
      assert_eq!(part_one(&inputs), 2);
    }

    #[test]
    fn test_part_two() {
      let inputs = get_input(4, true);
      assert_eq!(part_one(&inputs), 4);
    }    
}