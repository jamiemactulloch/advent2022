use common::{get_input};
use regex::Regex;

fn main() {
  let inputs = get_input(5, false);
  println!("Part one result = {}", part_one(&inputs));
  println!("Part two result = {}", part_two(&inputs));
}

#[derive(Debug)]
struct Move {
  count: usize,
  from: usize,
  to: usize
}

impl Move {
  pub fn from_string(input: &String) -> Self {
    let re = Regex::new(r"move (\d+) from (\d+) to (\d+)").unwrap();

    let captures = re.captures(input).unwrap();

    let (count, from, to) = (
        captures[1].parse::<usize>().unwrap(),
        captures[2].parse::<usize>().unwrap(),
        captures[3].parse::<usize>().unwrap(),
    );
 
    Move {count:count,from:from-1,to:to-1}
  }
}

#[derive(Debug)]
struct Stacks {
  stacks: Vec<Vec<char>>
}

impl Stacks {
  pub fn from_strings(input: &[String]) -> Self {
    let mut input: Vec<String> = input.into();
    let headers = input.pop().unwrap().replace(char::is_whitespace,"");
    let stack_count: usize = headers.chars().last().unwrap().to_digit(10).unwrap() as usize;
    input.reverse();
  
    let mut stacks: Vec<Vec<char>> = vec![Vec::<char>::new(); stack_count];
  
    for row in input {    
      for i in 0..stack_count {
        let chr = row.chars().nth((i*4)+1).expect("Parsing state row went awry.");
        if ('A'..='Z').contains(&chr) {
          stacks[i].push(chr);
        }
      }
    }

    Stacks { stacks: stacks }
  }

  pub fn move_one_at_a_time(&mut self, mv: &Move) {
    for _ in 0..mv.count {
      let item = self.stacks[mv.from].pop().unwrap();
      self.stacks[mv.to].push(item);
    }
  }

  pub fn apply_moves_one_at_a_time(&mut self, moves: &Vec<Move>) {
    for mv in moves.iter() {
      self.move_one_at_a_time(mv);
    }
  }

  pub fn move_all_at_once(&mut self, mv: &Move) {
    let from_stack = &mut self.stacks[mv.from];
    let index = from_stack.len() - mv.count;
    let subsection = (*from_stack).split_off(index);
    let to_stack = &mut self.stacks[mv.to];
    to_stack.extend(subsection.iter());
  }

  pub fn apply_moves_all_at_once(&mut self, moves: &Vec<Move>) {
    for mv in moves.iter() {
      self.move_all_at_once(mv);
    }
  }

  pub fn get_tops(&self) -> String {
    self.stacks.iter().map(|s| s.last().unwrap_or(&'&')).collect::<String>()
  }
}

fn part_one(inputs: &Vec<String>) -> String {
  let divider = inputs.iter().position(|s| s.is_empty()).expect("Well.. shit. This was formatted unexpectedly.");
  let (stacks_strings, move_strings) = inputs.split_at(divider);
  
  let mut stacks = Stacks::from_strings(stacks_strings);

  let mut move_strings: Vec<String> = move_strings.into();

  while (*move_strings.first().unwrap()).is_empty() {
    move_strings.remove(0);
  }

  let moves: Vec<Move> = move_strings
    .iter()
    .map(|s| Move::from_string(s))
    .collect();

    
  stacks.apply_moves_one_at_a_time(&moves);

  return stacks.get_tops();

}

fn part_two(inputs: &Vec<String>) -> String {
  let divider = inputs.iter().position(|s| s.is_empty()).expect("Well.. shit. This was formatted unexpectedly.");
  let (stacks_strings, move_strings) = inputs.split_at(divider);
  
  let mut stacks = Stacks::from_strings(stacks_strings);

  let mut move_strings: Vec<String> = move_strings.into();

  while (*move_strings.first().unwrap()).is_empty() {
    move_strings.remove(0);
  }

  let moves: Vec<Move> = move_strings
    .iter()
    .map(|s| Move::from_string(s))
    .collect();

    
  stacks.apply_moves_all_at_once(&moves);

  stacks.get_tops()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
      let inputs = get_input(5, true);
      assert_eq!(part_one(&inputs).as_str(), "CMZ");
    }

    #[test]
    fn test_part_two() {
      let inputs = get_input(5, true);
      assert_eq!(part_one(&inputs).as_str(), "MCD");
    }    
   
}