use common::get_input_single_line;
// use nom::{
//     branch::alt,
//     bytes::complete::tag,
//     character::complete as cc,
//     character::complete::{one_of, space1},
//     combinator::{map, value},
//     error::ParseError,
//     multi::separated_list1,
//     sequence::{preceded, tuple},
//     IResult,
// };

use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::{self as cc, alphanumeric1, multispace1, one_of},
    multi::{separated_list0, separated_list1},
    sequence::{delimited, pair, preceded, tuple},
    *,
};

#[derive(Debug, Copy, Clone)]
enum Term {
    Old,
    Constant(u64),
}

#[derive(Debug, Copy, Clone)]
enum Operation {
    Add(Term),
    Multiply(Term),
}

#[derive(Debug, Clone)]
struct Monkey {
    id: u64,
    items: Vec<u64>,
    operation: Operation,
    test_divisor: u64,
    true_monkey: u64,
    false_monkey: u64,
    inspections: u64
}

impl Monkey {
    fn parse(monkey_string: &str) -> IResult<&str, Monkey> {
        let (remainder_string, id) = delimited(tag("Monkey "), cc::u64, tag(":\n"))(monkey_string)?;
        let (remainder_string, _) = multispace1(remainder_string)?;
        let (remainder_string, items) = preceded(
            tag("Starting items: "),
            separated_list1(tag(", "), cc::u64),
        )(remainder_string)?;

        let (remainder_string, _) = multispace1(remainder_string)?;

        let (remainder_string, _) = tag("Operation: new = old ")(remainder_string)?;

        let (remainder_string, (operator, _, operand)) =
            tuple((one_of("*+"), multispace1, alphanumeric1))(remainder_string)?;

        let operation = if let Ok(int) = operand.parse::<u64>() {
            match operator {
                '+' => Some(Operation::Add(Term::Constant(int))),
                '*' => Some(Operation::Multiply(Term::Constant(int))),
                _ => None,
            }
        } else {
            match operator {
                '+' => Some(Operation::Add(Term::Old)),
                '*' => Some(Operation::Multiply(Term::Old)),
                _ => None,
            }
        }
        .unwrap();

        let (remainder_string, _) =
            pair(multispace1, tag("Test: divisible by "))(remainder_string)?;

        let (remainder_string, (test_divisor, _)) = pair(cc::u64, multispace1)(remainder_string)?;

        let (remainder_string, (_, true_monkey, _)) =
            tuple((tag("If true: throw to monkey "), cc::u64, multispace1))(remainder_string)?;

        let (remainder_string, (_, false_monkey)) =
            tuple((tag("If false: throw to monkey "), cc::u64))(remainder_string)?;

        Ok((
            remainder_string,
            Monkey {
                id,
                items,
                operation,
                test_divisor,
                true_monkey,
                false_monkey,
                inspections: 0
            },
        ))
    }

    fn receive_item(&mut self, item: u64) {
        self.items.push(item);
    }

    fn do_inspections(&mut self) {
        self.items.iter_mut().for_each(|i| {
            *i = match self.operation {
                Operation::Add(t) => match t {
                    Term::Old => *i + *i,
                    Term::Constant(n) => *i + n,
                },
                Operation::Multiply(t) => match t {
                    Term::Old => *i * *i,
                    Term::Constant(n) => *i * n,
                },
            }
        });
        self.inspections += self.items.len() as u64;
    }

    fn get_bored(&mut self) {
        self.items.iter_mut().for_each(|i| *i /= 3);
    }

    fn toss_items(&mut self) -> Vec<(u64, u64)> {
        let mut result = Vec::new();

        for item in self.items.iter() {
            if (item % self.test_divisor == 0) {
                result.push((self.true_monkey, *item));
            } else {
                result.push((self.false_monkey, *item));
            }
        }

        self.items = Vec::new();

        result
    }
}

fn main() {
    let input = get_input_single_line(11, false);
    println!("Part one result = {}", part_one(&input));
    println!("Part two result = {}", part_two(&input));
}

fn part_one(input: &String) -> u64 {
    let monkey_strings: Vec<&str> = input.split("\n\n").collect();
    let monkeys = monkey_strings
        .into_iter()
        .map(|monkey_string| Monkey::parse(monkey_string).unwrap())
        .map(|(_, monkey)| monkey)
        .collect::<Vec<Monkey>>();

    let out_monkeys = do_rounds(20,&monkeys, true);

    dbg!(&out_monkeys);

    let mut inspections = out_monkeys.iter().map(|m|m.inspections).collect::<Vec<u64>>();
    inspections.sort();

    let result = calculate_monkey_business(&out_monkeys);

    result
}

fn part_two(input: &String) -> u64 {
    let monkey_strings: Vec<&str> = input.split("\n\n").collect();
    let monkeys = monkey_strings
        .into_iter()
        .map(|monkey_string| Monkey::parse(monkey_string).unwrap())
        .map(|(_, monkey)| monkey)
        .collect::<Vec<Monkey>>();

    let out_monkeys = do_rounds(20,&monkeys, false);

    dbg!(&out_monkeys);

    let mut inspections = out_monkeys.iter().map(|m|m.inspections).collect::<Vec<u64>>();
    inspections.sort();

    let result = calculate_monkey_business(&out_monkeys);

    result
}

fn do_rounds(round_count: u64, monkeys: &Vec<Monkey>, get_bored: bool) -> Vec<Monkey> {
    let mut out_monkeys = monkeys.clone();
    for i in 1..=round_count {
        out_monkeys = do_round(&out_monkeys, get_bored);
    }
    out_monkeys
}

fn do_round(monkeys: &Vec<Monkey>, get_bored: bool) -> Vec<Monkey> {
    let mut out_monkeys = monkeys.clone();

    //let monkey = monkeys.first().unwrap();
    for monkey in monkeys.iter() {
        // inspect
        let equivalent_out_monkey = out_monkeys.iter_mut().find(|m| m.id == monkey.id).unwrap();
        equivalent_out_monkey.do_inspections();
        if get_bored {
            equivalent_out_monkey.get_bored();
        }

        let tossed_items = equivalent_out_monkey.toss_items();

        for (id, value) in tossed_items.iter() {
            let recipient_monkey = out_monkeys.iter_mut().find(|m| m.id == *id).unwrap();
            recipient_monkey.receive_item(*value);
        }
    }

    out_monkeys
}

fn calculate_monkey_business(monkeys: &Vec<Monkey>) -> u64 {
    let mut inspections = monkeys.iter().map(|m|m.inspections).collect::<Vec<u64>>();
    inspections.sort();

    let n = inspections.len()-1;
    let result = inspections[n] * inspections[n-1];

    result
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_one() {
        let input = get_input_single_line(7, true);
        assert_eq!(part_one(&input), 10605);
    }

    #[test]
    fn test_part_two() {
        let input = get_input_single_line(7, true);
        assert_eq!(part_two(&input), 113232);
    }

    #[test]
    fn actual_part_one() {
        let input = get_input_single_line(7, false);
        assert_eq!(part_one(&input), 1118405);
    }

    #[test]
    fn actual_part_two() {
        let input = get_input_single_line(7, false);
        assert_eq!(part_two(&input), 12545514);
    }
}
