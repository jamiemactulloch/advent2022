use std::fs;
use std::collections::HashSet;
use std::hash::Hash;
use std::cmp::Eq;
use std::ops::Mul;

pub fn get_input_single_line(day: usize, test: bool) -> String {
  let suffix = match test {
    true => "test",
    false => "actual"
  };
  let file_path = format!("inputs/{}_{}", day, suffix);
  let content_str = fs::read_to_string(file_path)
    .expect("Should have been able to read this!"); 

  content_str
}


pub fn get_input(day: usize, test: bool) -> Vec<String> {
  let single_line_input = get_input_single_line(day, test);

  let mut contents = single_line_input
    .split('\n')
    .map(String::from)
    .collect::<Vec<String>>();

  while (*contents.last().unwrap()).is_empty() {
    contents.truncate(contents.len()-1);
  }

  contents
}


pub fn dedupe_vec<T: Hash + Eq + Clone + Copy>(vec: &Vec<T>) -> Vec<T> {
  let mut set = HashSet::new();
  let mut deduped_vec = Vec::new();

  for &x in vec {
      if set.insert(x) {
          deduped_vec.push(x);
      }
  }

  deduped_vec
}

pub fn contains_duplicates<T: Eq + Copy>(vec: &[T]) -> bool {
  let mut temp = Vec::new();

  for &el in vec.iter() {
    if temp.contains(&el) {
      return true;
    }
    temp.push(el);
  }

  false

}

pub fn two_vector_shared_elements<T: Ord + Hash + Clone + Copy>(a: &Vec<T>, b: &Vec<T>) -> Vec<T> {
  let mut shared = Vec::new();

  let mut a = dedupe_vec(a);
  let mut b = (*b).clone();

  a.sort();
  b.sort();

  let mut a_iter = a.iter();
  let mut b_iter = b.iter();

  let mut a_next = a_iter.next();
  let mut b_next = b_iter.next();

  while a_next.is_some() && b_next.is_some() {
      let a_val = a_next.unwrap();
      let b_val = b_next.unwrap();

      match a_val.cmp(b_val) {
        std::cmp::Ordering::Equal => {
          shared.push(*a_val);
          a_next = a_iter.next();
          b_next = b_iter.next();
        },
        std::cmp::Ordering::Less => {
          a_next = a_iter.next();
        },
        std::cmp::Ordering::Greater => {
          b_next = b_iter.next();
        },
      }
  }

  shared
}

pub fn letter_to_int(c: char) -> Option<u32> {
    match c {
        'a'..='z' => Some((c as u32) - ('a' as u32) + 1),
        'A'..='Z' => Some((c as u32) - ('A' as u32) + 27),
        _ => None,
    }
}

pub fn two_matrix_product<T: Ord + Copy + Mul>(a: &[Vec<T>], b: &[Vec<T>]) -> Vec<Vec<T>> 
  where T: Mul<Output=T> {
  a.iter().cloned().zip(b.iter().cloned().into_iter())
  .map(|(x,y)|
    x.into_iter().zip(y.into_iter())
    .map(|(i,j)|i*j)
    .collect()
  )
  .collect()
}

pub fn many_matrix_product<T: Ord + Copy + Mul>(matrices: &[Vec<Vec<T>>]) -> Vec<Vec<T>>
  where T: Mul<Output=T> {
  matrices
    .iter()
    .cloned()
    .reduce(|acc,x| two_matrix_product(&acc,&x))
    .expect("Many matrix product failed.")
}

pub fn max_value_in_matrix<T: Ord + Copy>(matrix: &[Vec<T>]) -> T {
  *matrix
  .iter()
  .map(|v| v.iter().max().unwrap())
  .max()
  .unwrap()
}

pub fn two_vector_elementwise_or(a: &[bool], b: &[bool]) -> Vec<bool> {
  a
  .iter()
  .zip(b.iter())
  .map(|(x,y)| *x || *y)
  .collect()
}

pub fn many_vector_elementwise_or(vecs: Vec<Vec<bool>>) -> Vec<bool> {
 vecs
  .into_iter()
  .reduce(|acc,x| two_vector_elementwise_or(&acc,&x))
  .expect("Many vector bitwise element OR failed.")
}

pub fn transpose_matrix<T: Clone + Copy>(grid: &[Vec<T>]) -> Vec<Vec<T>> {
  let len = grid[0].len();
  let mut iters: Vec<_> = grid.iter().map(|n| n.iter()).collect();
  (0..len)
      .map(|_| {
          iters
              .iter_mut()
              .map(|n| *(n.next().unwrap()))
              .collect::<Vec<T>>()
      })
      .collect()
}

pub fn reverse_matrix_elementwise<T: Clone>(grid: &[Vec<T>]) -> Vec<Vec<T>> {
  grid
  .iter()
  .map(|row| row.clone().into_iter().rev().collect())
  .collect()
}

pub fn flatten_matrix<T: Clone>(grid: &Vec<Vec<T>>) -> Vec<T> {
  (*grid).clone()
  .into_iter()
  .flatten()
  .collect::<Vec<T>>()
}

pub fn get_index_of_char_from_matrix(grid: &Vec<Vec<char>>, chr: char) -> Option<(usize,usize)> {
  let (mut x, mut y) = (0,0);
  let mut found = false;

  for (i,a) in grid.iter().enumerate() {
    for (j,b) in a.iter().enumerate() {
      if *b == chr {
        x = j;
        y = i;
        found = true;
      }
    }
  }

  return if found {
    Some((x,y))
  }
  else {
    None
  }
}

pub fn replace_char_in_matrix(matrix: &mut Vec<Vec<char>>, old_char: char, new_char: char) {
  *matrix = (*matrix)
  .iter_mut()
  .map(|row| 
      row
      .iter_mut()
      .map(|&mut c| if c == old_char {new_char} else {c})
      .collect::<Vec<char>>()
  )
  .collect();
}

pub fn scale_up_row<T: Copy>(row: &Vec<T>, scale: u16) -> Vec<T> {
  let mut result = Vec::new();
  for &el in row.iter() {
    for _ in 0..scale {
      result.push(el);
    }
  }
  result
}

pub fn scale_up_matrix<T: Copy>(grid: &Vec<Vec<T>>, scale: u16) -> Vec<Vec<T>> {
  let mut result = Vec::new();

    for row in grid.clone().into_iter() {
      for _ in 0..=scale {
        let row_scaled = scale_up_row(&row, scale);
        result.push(row_scaled);
      }
    }

    result
}